PHP + Apache + mod_auth_cas
---------------------------

This image is built on a basic PHP + Apache image, and adds support for the University of Arizona's CAS implementation, WebAuth.

It is probably easiest to simple copy this repository, and modify it for your needs, then upload it to a new repository if you wish.

Clone this repository to a new location:

    git clone https://bitbucket.org/ua-uits-solarch/docker-phpwebauth.git newapp

To disconnect the checked out repository from the git origin, simply remove the .git directory. You can then init a new repository if you wish to check it in somewhere else.

You'll have a directory structure like:

    /yourapp
        apache/
        app/
        bin/
        php/
        Dockerfile

In the apache directory, put an updated version of the app.conf file from this project. Customize it with your server name, and change the CAS settings if needed.

Put your PHP application files inside of the app/ directory.

Modify the Dockerfile from this project and include whatever additional php modules, or setup steps needed for your application.

To add new PHP modules to your image, the underlying base image has provided a script docker-php-ext-install which does this. See https://github.com/docker-library/docs/blob/master/php/README.md for details.

For example to add pdo_mysql support, add this to your Dockerfile:

    # Install MySQL PDO
    RUN docker-php-ext-install pdo_mysql


To build your image:

    docker build -t yourproj/yourapp:latest .

**IMPORTANT**
In order for CAS to correctly determine the callback URL, you must set a hostname on the container that matches the domain in your application URL.

To run your application with something like:

    docker run -d --name yourapp -p 80:80 -h yourapp.example.com yourproj/yourapp

That will start a new container, and map port 80 on the host to port 80 inside the container, where apache is running your application.  Change your host port if needed.


Environment Variables
---------------------

Environment variables can be passed into the docker run command in such a way that they are added to the apache vhost config and thus available to PHP.  Simply pass in environment variables with a PHP_ prefix. Those will have the PHP_ prefix stripped off, and added to the app.conf vhost config in apache.

    docker run -d --name yourapp -p 80:80 -h yourapp.example.com -e "PHP_db_user=ausername" -e "PHP_db_pass=secret" yourproj/yourapp

This will be parsed by the bin/propagate_environment.pl script, and the resulting app.conf will look something like:

    <Location />
        AuthType CAS
        AuthName webauth
        require valid-user
    
        SetEnv db_user ausername
        SetEnv db_pass secret
    </Location>

And thus be available within your PHP code as:

    $_SERVER['db_user'];
    $_SERVER['db_pass'];